@extends('layouts.vendor')

@section('styles')

<link href="{{asset('assets/admin/css/jquery-ui.css')}}" rel="stylesheet" type="text/css">

@endsection


@section('content')

            <div class="content-area">

              <div class="mr-breadcrumb">
                <div class="row">
                  <div class="col-lg-12">
                      <h4 class="heading">{{ __('Add New Zones') }} <a class="add-btn" href="{{route('vendor-zone')}}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h4>
                      <ul class="links">
                        <li>
                          <a href="{{ route('vendor-zone') }}">{{ __('Zones') }}</a>
                        </li>
                        <li>
                          <a href="{{ route('vendor-zones-map') }}">{{ __('Add New Zone') }}</a>
                        </li>
                      </ul>
                  </div>
                </div>
              </div>

              <div class="add-product-content">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="product-description">
                      <div class="body-area">
                        <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
                        @include('includes.admin.form-both') 
                      <form id="geniusform" action="{{route('vendor-zones-map')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="zone_id" class="zone_id">
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Code') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <select id="selectZone">
                            	<option selected>-- Select Zone --</option>
                            	@foreach($zones as $zone)
                            	 <option value="{{ $zone->ID }}">{{ $zone->CODE }}</option>
                            	@endforeach
                            </select>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Name') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field name" name="name" placeholder="{{ __('Enter Name') }}" required="" value="">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Description') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field description" name="description" placeholder="{{ __('Enter Description') }}">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Google Areas') }} *</h4>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field googleArea" name="name" placeholder="{{ __('Enter Name') }}" required="" value="">
                          </div>
                        </div>
                        <br>
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                            </div>
                          </div>
                          <div class="col-lg-7"> 
                            <button class="addProductSubmit-btn" type="submit">{{ __('Create Zone') }}</button>
                          </div>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

@endsection

@section('scripts')

<script type="text/javascript">
	$(document).ready(function(){
       $("#selectZone").on("change" , function(){
          $(".zone_id").val('');
          $(".name").val('');
          $(".description").val('');
          $(".googleArea").val('');
          val = $(this).val();
           $.ajax({
              url: "{{ route('get-zones') }}/" + val,
              type: "GET",
              success: function (data) {
                $(".zone_id").val(data.ID);
                $(".name").val(data.NAME);
                $(".description").val(data.DESCRIPTION);
                $(".googleArea").val(data.GOOGLE_AREA);
              }
            });
       });
	});
</script>

@endsection



