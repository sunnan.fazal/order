@extends('layouts.vendor') 

@section('content')  
        <input type="hidden" id="headerdata" value="{{ __('ZONES') }}">
        <div class="content-area">
          <div class="mr-breadcrumb">
            <div class="row">
              <div class="col-lg-12">
                  <h4 class="heading">{{ __('Zones') }}</h4>
              </div>
            </div>
          </div>
          <div class="product-area">
            <div class="row">
              <div class="col-lg-12">
                <div class="mr-table allproduct">

                    @include('includes.admin.form-success')  

                  <div class="table-responsiv">
                      <table id="geniustable" class="table table-hover dt-responsive" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                                        <th>{{ __('Code') }}</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Description') }}</th>
                                        <th>{{ __('Google Area') }}</th>
                                        <th></th>
                          </tr>
                        </thead>
                      </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection    



@section('scripts')


{{-- DATA TABLE --}}

<script type="text/javascript">

  var table = $('#geniustable').DataTable({
       ordering: false,
           processing: true,
           serverSide: true,
           ajax: '{{ route('vendor-zones-datatables') }}',
           columns: [
                    { data: 'CODE', name: 'Code' },
                    { data: 'NAME', name: 'Name' },
                    { data: 'DESCRIPTION', name: 'Description' },
                    { data: 'GOOGLE_AREA', name: 'Google Area' },
                    { data: 'action', searchable: false, orderable: false }

                 ],
            language : {
              processing: '<img src="{{asset('assets/images/'.$gs->admin_loader)}}">'
            },
      drawCallback : function( settings ) {
            $('.select').niceSelect();  
      }
        });

    $(function() {
    $(".btn-area").append('<div class="col-sm-4 table-contents">'+
      '<a class="add-btn" href="{{route('vendor-zones-map')}}">'+
      '<i class="fas fa-plus"></i> {{ __('Add New Zone') }}'+
      '</a>'+
      '</div>');
  });                     
                


{{-- DATA TABLE ENDS--}}


</script>





@endsection   