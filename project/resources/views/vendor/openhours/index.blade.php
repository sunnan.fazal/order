@extends('layouts.vendor')

@section('content')
    <input type="hidden" id="headerdata" value="{{ strtoupper($langg->CUST_VEND_OPEN_HOURS) }}">
    <div class="content-area">
        <div class="mr-breadcrumb">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="heading">{{ $langg->CUST_VEND_OPEN_HOURS }}</h4>
                    <ul class="links">
                        <li>
                            <a href="{{ route('vendor-dashboard') }}">{{ $langg->lang441 }} </a>
                        </li>
                        <li>
                            <a href="javascript:;">{{ $langg->lang452 }}</a>
                        </li>
                        <li>
                            <a href="{{ route('vendor-open-hours') }}">{{ $langg->CUST_VEND_OPEN_HOURS }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="product-area">
            <div class="row">
                <div class="col-sm-12 mr-table">
                    <div class="alert alert-warning" style="display: flex">
                        <h6>Active store items in close timings : </h6> &nbsp;&nbsp;<input type="checkbox" id="activeItems" style="margin-top: 4px" {{ ($checkbox) ? 'checked' : '' }}>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="mr-table alltimings">
                        @include('includes.admin.form-success')

                        <div class="table-responsiv">
                            <table id="geniustable" class="table table-hover dt-responsive" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>{{ $langg->CUST_VEND_WEEKDAY }}</th>
                                    <th>{{ $langg->CUST_VEND_FROM }}</th>
                                    <th>{{ $langg->CUST_VEND_TO }}</th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- ADD / EDIT MODAL --}}
        @include("vendor.openhours.open-hours-modal")
    {{-- ADD / EDIT MODAL ENDS --}}

@endsection

@section('scripts')


    {{-- DATA TABLE --}}

    <script type="text/javascript">

        var table = $('#geniustable').DataTable({
            ordering: false,
            processing: true,
            serverSide: true,
            ajax: '{{ route('vendor-open-hours-datatables') }}',
            columns: [
                { data: 'weekday', name: 'weekday' },
                { data: 'from_timing', name: 'from_timing' },
                { data: 'to_timing', name: 'to_timing' },
                { data: 'action', searchable: false, orderable: false }
            ],
            language : {
                processing: '<img src="{{asset('assets/images/'.$gs->admin_loader)}}">'
            }
        });

        $(function() {
            $(".btn-area").append('<div class="col-sm-4 table-contents">'+
                '<a class="add-btn" data-href="{{route('vendor-open-hours-create')}}" id="add-data" data-toggle="modal" data-target="#modal1">'+
                '<i class="fas fa-plus"></i> {{ $langg->CUST_VEND_HOUR_CREATE }}'+
                '</a>'+
                '</div>');
        });

        {{-- DATA TABLE ENDS--}}
        $(document).ready(function () {
            $("body").on("change" , "#activeItems" , function () {
                $.ajax({
                    type: "GET",
                    url: '{{ route('vendor-profile-update') }}',
                    data : {
                        active : ($(this).is(":checked")) == true ? 1 : 0 ,
                    }
                }).done(function(result) {
                    console.log(result);
                }).fail(function() {
                    alert('error');
                });
            });
        });
    </script>

@endsection