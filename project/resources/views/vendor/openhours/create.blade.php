@extends('layouts.load')

@section('content')

    <div class="content-area">
        <div class="add-product-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="product-description">
                        <div class="body-area" id="modalEdit">
                            @include('includes.admin.form-error')
                            <form id="geniusformdata" class="storetimingformdata" action="{{ route('vendor-open-hours-store') }}" method="POST">
                                <input type="hidden" class="url" value="{{ route('vendor-open-hours-validate') }}">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                            <h4 class="heading">{{ $langg->CUST_VEND_WEEKDAY }} *</h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <select name="weekday" id="weekday" required>
                                            <option value="">-- Select Weekday --</option>
                                            @foreach($weekdays as $days)
                                                <option value="{{ $days->id }}">{{ $days->weekday }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                            <h4 class="heading">{{ $langg->CUST_VEND_FROM }} </h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <select name="from" id="from" required>
                                            <option value="">-- Select Timing --</option>
                                            @foreach($timing as $time)
                                                <option value="{{ $time->timing }}">{{ $time->timing }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                            <h4 class="heading">{{ $langg->CUST_VEND_TO }}</h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <select name="to" id="to" required>
                                            <option value="">-- Select Timing --</option>
                                            @foreach($timing as $time)
                                                <option value="{{ $time->timing }}">{{ $time->timing }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <button class="addProductSubmit-btn" type="submit">{{ $langg->lang739 }}</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="alert alert-danger formResult" style="display: none">
                                            <h6></h6>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection