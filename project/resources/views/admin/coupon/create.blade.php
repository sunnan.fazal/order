@extends('layouts.admin')

@section('styles')

<link href="{{asset('assets/admin/css/jquery-ui.css')}}" rel="stylesheet" type="text/css">

@endsection


@section('content')

            <div class="content-area">

              <div class="mr-breadcrumb">
                <div class="row">
                  <div class="col-lg-12">
                      <h4 class="heading">{{ __('Add New Coupon') }} <a class="add-btn" href="{{route('admin-coupon-index')}}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h4>
                      <ul class="links">
                        <li>
                          <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
                        </li>
                        <li>
                          <a href="{{ route('admin-coupon-index') }}">{{ __('Coupons') }}</a>
                        </li>
                        <li>
                          <a href="{{ route('admin-coupon-create') }}">{{ __('Add New Coupon') }}</a>
                        </li>
                      </ul>
                  </div>
                </div>
              </div>

              <div class="add-product-content">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="product-description">
                      <div class="body-area">
                        <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
                        @include('includes.admin.form-both') 
                      <form id="geniusform" action="{{route('admin-coupon-store')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Code') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="code" placeholder="{{ __('Enter Code') }}" required="" value="">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Type') }} *</h4>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <select id="type" class="discountType" name="type" required="">
                                <option value="">{{ __('Choose a type') }}</option>
                                <option value="1">{{ __('General Discount') }}</option>
                                <option value="2">{{ __('Specific Discount') }}</option>
                              </select>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Discount') }} *</h4>
                            </div>
                          </div>
                          <div class="col-lg-7">
                              <select id="discount" class="discountNature" name="discount" required="">
                                <option value="">{{ __('Choose a type') }}</option>
                                <option value="1">{{ __('Cart Discount %') }}</option>
                                <option value="2">{{ __('Cart Discount $') }}</option>
                              </select>
                          </div>
                        </div>

                        <div class="row hidden">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading"></h4>
                            </div>
                          </div>
                          <div class="col-lg-3">
                            <input type="text" class="input-field less-width" name="price" placeholder="" required="" value=""><span></span>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Start Date') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="start_date" id="from" placeholder="{{ __('Select a date') }}" required="" value="">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('End Date') }} *</h4>
                                <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="end_date" id="to" placeholder="{{ __('Select a date') }}" required="" value="">
                          </div>
                        </div>
                        <div class="row GeneralHide">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Minimum Order Price') }} *</h4>
                                <p class="sub-heading">{{ __('(Minimum cart amount that requires before coupon applies)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="minimum_price" id="minimum_price" placeholder="Mininum Price" value="">
                          </div>
                        </div>
                        <div class="row GeneralHide">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Include Product Catalog') }} *</h4>
                                <p class="sub-heading">{{ __('(Include Product Catalog to apply coupon on)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <select class="input-field includeProducts" name="includeProducts[]" multiple="multiple" >
                              @foreach($inc_product as $prod)
                                <option value="{{ $prod->id }}">{{ $prod->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="row GeneralHide">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Exclude Product Catalog') }} *</h4>
                                <p class="sub-heading">{{ __('(Exclude Product Catalog to apply coupon on)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <select class="input-field excludeProducts" name="excludeProducts[]" multiple="multiple" >
                              @foreach($exc_product as $prod)
                                <option value="{{ $prod->id }}">{{ $prod->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="row GeneralHide">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Include Categories') }} *</h4>
                                <p class="sub-heading">{{ __('(Include Categories to apply coupon on)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <select class="input-field includeCategories" name="includeCategories[]" multiple="multiple" >
                              @foreach($inc_categories as $cat)
                                <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="row GeneralHide">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Exclude Categories') }} *</h4>
                                <p class="sub-heading">{{ __('(Exclude Categories to apply coupon on)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <select class="input-field excludeCategories" name="excludeCategories[]" multiple="multiple" >
                              @foreach($exc_categories as $cat)
                                <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Usage Limit') }} *</h4>
                                <p class="sub-heading">{{ __('(How many times the coupon can be used in total)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="usage_limit" id="usage_limit" placeholder="Usage Limit for this coupon" value="">
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __('Usage Limit Per User') }} *</h4>
                                <p class="sub-heading">{{ __('(How many times the coupon can be used per customer)') }}</p>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" class="input-field" name="usage_limit_user" id="usage_limit_customer" placeholder="Usage Limit for user"  value="">
                          </div>
                        </div>

                        <br>
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                              
                            </div>
                          </div>
                          <div class="col-lg-7"> 
                            <button class="addProductSubmit-btn" type="submit">{{ __('Create Coupon') }}</button>
                          </div>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

@endsection

@section('scripts')

<script type="text/javascript">

  $(document).ready(function() {
    $('.includeProducts').select2();
    $('.excludeProducts').select2();
    $('.includeCategories').select2();
    $('.excludeCategories').select2();
  });

    $('.discountNature').on('change', function() {
      var val = $(this).val();
      var selector = $(this).parent().parent().next();
      if(val == "")
      {
        selector.hide();
      }
      else {
        if(val == 1)
        {
          selector.find('.heading').html('{{ __('Percentage') }} *');
          selector.find('input').attr("placeholder", "{{ __('Enter Percentage') }}").next().html('%');
          selector.css('display','flex');
        }
        else if(val == 2){
          selector.find('.heading').html('{{ __('Amount') }} *');
          selector.find('input').attr("placeholder", "{{ __('Enter Amount') }}").next().html('$');
          selector.css('display','flex');
        }
      }
    });

    $('.discountType').on('change', function() {
      var val = $(this).val();
      if(val == "")
      {
        $(".GeneralHide").hide();
      }
      else {
        if(val == 1)
        {
          $(".GeneralHide").css('display','none');
        }
        else if(val == 2){
          $(".GeneralHide").css('display','flex');
        }
      }
    });

  $(document).on("change", "#times" , function(){
    var val = $(this).val();
    var selector = $(this).parent().parent().next();
    if(val == 1){
    selector.css('display','flex');
    }
    else{
    selector.find('input').val("");
    selector.hide();    
    }
});

</script>

<script type="text/javascript">
    var dateToday = new Date();
    var dates =  $( "#from,#to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        changeYear: true,
        minDate: dateToday,
        onSelect: function(selectedDate) {
        var option = this.id == "from" ? "minDate" : "maxDate",
          instance = $(this).data("datepicker"),
          date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
          dates.not(this).datepicker("option", option, date);
    }
});
</script>

@endsection

