<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Zones;
use Datatables;
use Illuminate\Http\Request;

class ZonesController extends Controller
{
	public function index()
	{
		return view('admin.zones.index');
	}

	public function datatables()
    {
         $datas = Zones::orderBy('ID','desc')->get();
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
             ->addColumn('action', function(Zones $data) {
                 return '<div class="action-list"><a href="' . route('admin-coupon-edit',$data->ID) . '"> <i class="fas fa-edit"></i>Edit</a><a href="javascript:;" data-href="' . route('admin-coupon-delete',$data->id) . '" data-toggle="modal" data-target="#confirm-delete" class="delete"><i class="fas fa-trash-alt"></i></a></div>';
             })
             ->toJson(); //--- Returning Json Data To Client Side
    }

    public function create()
    {
        return view('admin.zones.create');
    }

    public function store(Request $request)
    {

    	$obj = new Zones();
    	$obj->CODE = $request->get("code");
    	$obj->NAME = $request->get("name");
    	$obj->DESCRIPTION = $request->get("description");
    	$obj->GOOGLE_AREA = $request->get("googleAreas");
    	$obj->save();

    	$msg = 'New Zone Added Successfully.';
        return response()->json($msg);      
    }






}
?>