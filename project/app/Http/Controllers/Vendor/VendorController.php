<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Http\Middleware\Vendor;
use App\Models\Category;
use App\Models\Generalsetting;
use App\Models\Subcategory;
use App\Models\Timings;
use App\Models\VendorOrder;
use App\Models\VendorZones;
use App\Models\Verification;
use App\Models\Weekday;
use App\Models\Zones;
use Auth;
use DB;
use Datatables;
use Facades\App\Models\VendorProfile;
use Facades\App\Models\VendorStoreTiming;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;

class VendorController extends Controller
{

    public $lang;
    public function __construct()
    {

        $this->middleware('auth');

            if (Session::has('language')) 
            {
                $data = DB::table('languages')->find(Session::get('language'));
                $data_results = file_get_contents(public_path().'/assets/languages/'.$data->file);
                $this->lang = json_decode($data_results);
            }
            else
            {
                $data = DB::table('languages')->where('is_default','=',1)->first();
                $data_results = file_get_contents(public_path().'/assets/languages/'.$data->file);
                $this->lang = json_decode($data_results);
                
            } 
    }

    //*** GET Request
    public function index()
    {
        $user = Auth::user();  
        $pending = VendorOrder::where('user_id','=',$user->id)->where('status','=','pending')->get(); 
        $processing = VendorOrder::where('user_id','=',$user->id)->where('status','=','processing')->get(); 
        $completed = VendorOrder::where('user_id','=',$user->id)->where('status','=','completed')->get();
        $hasTiming = VendorStoreTiming::specificVendor()->count() > 0;
        return view('vendor.index',compact('user','pending','processing','completed' , 'hasTiming'));
    }

    public function zones()
    {
        $zones = Zones::all();
        return view("vendor.zone" , compact("zones"));
    }
    public function mappingZones()
    {
        $zones = Zones::all();
        return view("vendor.create-zone" , compact("zones"));
    }

    public function getZones($id)
    {
      return Zones::where("ID" , $id)->first()->toArray();
    }

    public function saveVendorZone(Request $request)
    {
        if($request->get("zone_id") != "")
        {
           $obj = new VendorZones();
           $obj->ZONE_ID = $request->get("zone_id");
           $obj->VENDOR_ID = Auth::user()->id;
           $obj->save();
           return response()->json("Zone Added Successfully !"); 
        }
        return response()->json(array('errors' => 'Zone must be selected !'));
    }

    public function datatables()
    {
        $data = VendorZones::with(["zones"])->where("vendor_id" , Auth::user()->id)->get();
        return DataTables::of($data)
            ->editColumn('CODE' , function ($data){
                return $data->zones->CODE;
            })
            ->editColumn('NAME' , function ($data){
                return $data->zones->NAME;
            })
            ->editColumn('DESCRIPTION' , function ($data){
                return $data->zones->DESCRIPTION;
            })
            ->editColumn('GOOGLE_AREA' , function ($data){
                return $data->zones->GOOGLE_AREA;
            })
            ->addColumn('action', function($data) {
                return '<div class="action-list"><a href="'.route('vendor-zones-remove' , $data->ID).'" class="delete"><i class="fas fa-trash"></i>Delete</a></div>';
            })
            ->rawColumns(['action'])
            ->toJson();
    }

    public function removeZones($id)
    {
       VendorZones::destroy($id);
       return redirect()->back();
    }

    public function profileupdate(Request $request)
    {
        //--- Validation Section
        $rules = [
               'shop_image'  => 'mimes:jpeg,jpg,png,svg',
                ];

        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        $input = $request->all();  
        $data = Auth::user();    

        if ($file = $request->file('shop_image')) 
         {      
            $name = time().$file->getClientOriginalName();
            $file->move('assets/images/vendorbanner',$name);           
            $input['shop_image'] = $name;
        }

        $data->update($input);
        $msg = 'Successfully updated your profile';
        return response()->json($msg); 
    }

    // Spcial Settings All post requests will be done in this method
    public function socialupdate(Request $request)
    {
        //--- Logic Section
        $input = $request->all(); 
        $data = Auth::user();   
        if ($request->f_check == ""){
            $input['f_check'] = 0;
        }
        if ($request->t_check == ""){
            $input['t_check'] = 0;
        }

        if ($request->g_check == ""){
            $input['g_check'] = 0;
        }

        if ($request->l_check == ""){
            $input['l_check'] = 0;
        }
        $data->update($input);
        //--- Logic Section Ends
        //--- Redirect Section        
        $msg = 'Data Updated Successfully.';
        return response()->json($msg);      
        //--- Redirect Section Ends                

    }

    //*** GET Request
    public function profile()
    {
        $data = Auth::user();  
        return view('vendor.profile',compact('data'));
    }

    //*** GET Request
    public function ship()
    {
        $gs = Generalsetting::find(1);
        if($gs->vendor_ship_info == 0) {
            return redirect()->back();
        }
        $data = Auth::user();  
        return view('vendor.ship',compact('data'));
    }

    //*** GET Request
    public function banner()
    {
        $data = Auth::user();  
        return view('vendor.banner',compact('data'));
    }

    //*** GET Request
    public function social()
    {
        $data = Auth::user();  
        return view('vendor.social',compact('data'));
    }

    //*** GET Request
    public function openHours()
    {
        $data = Auth::user();
        $activeTimes = VendorProfile::specificVendor();
        $checkbox = null;
        if($activeTimes->count() > 0)
        {
            $activeTimes = $activeTimes->first();
            $checkbox = $activeTimes->active_item;
        }
        return view('vendor.openhours.index',compact('data','checkbox'));
    }

    //*** GET Request
    public function openHoursAll()
    {
        $data = VendorStoreTiming::with(["weekday"])->where("vendor_id" , Auth::user()->id)->orderBy("weekday_id" , "ASC")->get();
        return DataTables::of($data)
            ->editColumn('from_timing' , function ($data){
                return $data->from_timing;
            })
            ->editColumn('to_timing', function ($data){
                return $data->to_timing;
            })
            ->addColumn('weekday', function ($data){
                return $data->weekday->weekday;
            })
            ->addColumn('action', function($data) {
                return '<div class="action-list"><a href="'.route('vendor-open-hours-remove' , $data->id).'" class="delete"><i class="fas fa-trash"></i>Delete</a></div>';
            })
            ->rawColumns(['action'])
            ->toJson();
    }

    public function openHoursValidate(Request $request)
    {
        $vendors = VendorStoreTiming::specificVendor()->get();
        $error = 0;
        foreach ($vendors as $vend)
        {
            if($vend->weekday_id == $request->get("weekday"))
            {
                $from = \DateTime::createFromFormat('H:i A', $request->get("from"));
                $date2 = \DateTime::createFromFormat('H:i A', $vend->from_timing);
                $date3 = \DateTime::createFromFormat('H:i A', $vend->to_timing);
                if ($from > $date2 && $from < $date3)
                {
                    $error = 1;
                }

                $to = \DateTime::createFromFormat('H:i A', $request->get("to"));
                $date2 = \DateTime::createFromFormat('H:i A', $vend->from_timing);
                $date3 = \DateTime::createFromFormat('H:i A', $vend->to_timing);
                if ($to > $date2 && $to < $date3)
                {
                    $error = 1;
                }

                if($request->get("from") == $vend->from_timing && $request->get("to") == $vend->to_timing)
                {
                    $error = 1;
                }
            }
        }
        if($request->get("from") == $request->get("to"))
        {
            $error = 1;
        }
        return $error;
    }

    public function openHoursDelete($id = "")
    {
        if($id != "")
        {
            VendorStoreTiming::whereId($id)->delete();
            return redirect()->back();
        }
        else{
            return redirect()->back();
        }

    }

    //*** GET Request
    public function openHoursCreate()
    {
        $weekdays = Weekday::all();
        $timing = Timings::all();
        return view('vendor.openhours.create',compact('weekdays','timing'));
    }

    //*** POST Request
    public function saveOpenHours(Request $request)
    {
        if( $this->openHoursValidate($request) == 0)
        {
            return VendorStoreTiming::saveStoreTime($request);
        }
    }

    //*** GET Request
    public function vendorProfileUpdate(Request $request)
    {
        VendorProfile::updateProfile($request);
    }

    //*** GET Request
    public function subcatload($id)
    {
        $cat = Category::findOrFail($id);
        return view('load.subcategory',compact('cat'));
    }

    //*** GET Request
    public function childcatload($id)
    {
        $subcat = Subcategory::findOrFail($id);
        return view('load.childcategory',compact('subcat'));
    }

    //*** GET Request
    public function verify()
    {
        $data = Auth::user();  
        if($data->checkStatus())
        {
            return redirect()->back();
        }
        return view('vendor.verify',compact('data'));
    }

    //*** GET Request
    public function warningVerify($id)
    {
        $verify = Verification::findOrFail($id);
        $data = Auth::user();  
        return view('vendor.verify',compact('data','verify'));
    }

    //*** POST Request
    public function verifysubmit(Request $request)
    {
        //--- Validation Section
        $rules = [
          'attachments.*'  => 'mimes:jpeg,jpg,png,svg|max:10000'
           ];
        $customs = [
            'attachments.*.mimes' => 'Only jpeg, jpg, png and svg images are allowed',
            'attachments.*.max' => 'Sorry! Maximum allowed size for an image is 10MB',
                   ];

        $validator = Validator::make(Input::all(), $rules,$customs);
        
        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        $data = new Verification();
        $input = $request->all();

        $input['attachments'] = '';
        $i = 0;
                if ($files = $request->file('attachments')){
                    foreach ($files as  $key => $file){
                        $name = time().$file->getClientOriginalName();
                        if($i == count($files) - 1){
                            $input['attachments'] .= $name;
                        }
                        else {
                            $input['attachments'] .= $name.',';
                        }
                        $file->move('assets/images/attachments',$name);

                    $i++;
                    }
                }
        $input['status'] = 'Pending';        
        $input['user_id'] = Auth::user()->id;
        if($request->verify_id != '0')
        {
            $verify = Verification::findOrFail($request->verify_id);
            $input['admin_warning'] = 0;
            $verify->update($input);
        }
        else{

            $data->fill($input)->save();
        }

        //--- Redirect Section        
        $msg = '<div class="text-center"><i class="fas fa-check-circle fa-4x"></i><br><h3>'.$this->lang->lang804.'</h3></div>';
        return response()->json($msg);      
        //--- Redirect Section Ends     
    }

}
