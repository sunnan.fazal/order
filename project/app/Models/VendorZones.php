<?php

namespace App\Models;

use App\Models\User;
use App\Models\Zones;
use Illuminate\Database\Eloquent\Model;

class VendorZones extends Model
{
	protected $table = 'vend_zones';
    protected $primaryKey = 'ID';
    protected $fillable = ["ID","ZONE_ID","VENDOR_ID"];
    public $timestamps = false;


    public function zones()
    {
    	return $this->belongsTo(Zones::class , 'ZONE_ID' , "ID");
    }
    public function vendors()
    {
    	return $this->belongsTo(User::class , 'VENDOR_ID' , "id");
    }

}
