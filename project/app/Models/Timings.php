<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Timings extends Model
{
    protected $table = 'timings';
    protected $primaryKey = 'id';
    public $timestamps = false;
}