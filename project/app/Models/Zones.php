<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Zones extends Model
{
	protected $table = 'zones';
    protected $primaryKey = 'ID';
    protected $fillable = ["CODE","NAME","DESCRIPTION","GOOGLE_AREA"];
    public $timestamps = false;
}
