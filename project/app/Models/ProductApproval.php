<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProductApproval extends Model
{
    protected $table = 'product_approval_admin';
    protected $primaryKey = 'id';
    protected $fillable = ["product_catalog_id" , "approval_id" , "approved","approve_date","created_at"];
    public $timestamps = false;

    public function saveProductWorkflow($productId)
    {
        $obj = new self();
        $obj->product_catalog_id = $productId;
        $obj->approved = 0;
        $obj->created_at = date("Y-m-d H:i:s");
        $obj->save();
    }

}