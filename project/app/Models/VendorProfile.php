<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class VendorProfile extends Model
{
    protected $table = 'vendor_profile';
    protected $primaryKey = 'id';
    protected $fillable = ['vendor_id','active_item'];
    public $timestamps = false;


    public function updateProfile($request)
    {
        $vendor = Auth::user()->id;
        $profile = $this->exists($vendor);
        if($profile == 0)
        {
            $profile = new self();
            $profile->vendor_id = $vendor;
            $profile->active_item = $request->get("active");
            $profile->save();
        }
        else{
            $this->where("id" , $profile)->update(["active_item" => $request->get("active")]);
        }
    }

    public function scopeSpecificVendor($query)
    {
        return $query->where("vendor_id" , Auth::user()->id);
    }

    public function exists($id)
    {
        $q = $this->where("vendor_id" , $id);
        if($q->count() > 0)
        {
            return $q->first()->id;
        }
        return 0;
    }
}