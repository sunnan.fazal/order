<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class VendorStoreTiming extends Model
{
    protected $table = 'vendor_store_timings';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ["weekday_id","from_timing","to_timing","vendor_id"];

    public function weekday()
    {
        return $this->belongsTo(Weekday::class , 'weekday_id','id');
    }

    public function saveStoreTime($request)
    {
        $vendor = Auth::user()->id;
        $obj = new self();
        $obj->weekday_id = $request->input("weekday");
        $obj->from_timing = $request->input("from");
        $obj->to_timing = $request->input("to");
        $obj->vendor_id = $vendor;
        echo $obj->save();
    }

    public function scopeSpecificVendor($query)
    {
        return $query->where("vendor_id" , Auth::user()->id);
    }

    public function exists($weekday, $vendor)
    {
        return ! ($this->where('weekday_id' , $weekday)->where('vendor_id',$vendor)->count() > 0);
    }
}