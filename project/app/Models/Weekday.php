<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Weekday extends Model
{
    protected $table = 'weekday';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function stores()
    {
        return $this->hasMany(VendorStoreTiming::class , "weekday_id",'id');
    }
}