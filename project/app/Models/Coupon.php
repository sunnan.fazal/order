<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
	protected $primaryKey = 'id';
	protected $table = 'coupon';
    protected $fillable = ['code', 'type'  , 'discount_type' , 'discount', 'times_used','min_price','inc_product','exc_product','inc_categories','exc_categories','usage_limit','usage_limit_user', 'start_date','end_date','zones'];
    public $timestamps = false;
}
