$(document).ready(function () {
    $("body .storetimingformdata").on('change','#weekday,#from,#to',function () {
        weekday = $("body .storetimingformdata #weekday").val();
        from = $("body .storetimingformdata #from").val();
        to = $("body .storetimingformdata #to").val();
        url = $("body .storetimingformdata .url").val();
        val = null;
        $("body .storetimingformdata .formResult").css("display","none");
        if(weekday != "" && from != "" && to != "")
        {
            $.ajax({
                type: "GET",
                url: url,
                data        : {
                    from       : from,
                    to         : to,
                    weekday    : weekday
                }
            }).done(function(result) {
                if (result == 1){
                    $("body .storetimingformdata .formResult").css("display","block").children("h6").html("Weekday timing is invalid !");
                }
            }).fail(function() {
                alert('error');
            });
        }
    });

});
